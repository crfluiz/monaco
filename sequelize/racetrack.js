// const { Sequelize, Model, DataTypes } = require('sequelize');
// const sequelize = new Sequelize('postgres://exampleuser:examplepass@postgres:5432/corrida_example');

const { sequelize, Model, DataTypes } = require('./config');

class RaceTrack extends Model {}
RaceTrack.init({
  name: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  length: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  curves: {
    type: DataTypes.DECIMAL,
    allowNull: false
  },
  region: {
    type: DataTypes.STRING
  }
}, { sequelize, modelName: 'racetrack' });

(async () => {
  await sequelize.sync({ alter: true });
})();

module.exports = RaceTrack;