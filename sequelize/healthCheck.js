const { sequelize } = require('./config');
module.exports = async () => {
    try {
        await sequelize.authenticate();
        return { ok: true, message: 'Connection has been established successfully.' }
    } catch (error) {
        console.error(error);
        return { ok: false, error }
    }
}