const request = require('supertest');
const app = require('../index');

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect({ok: true}, done);
  });
}); 
